import React from 'react'

import icon_logo from '../static/pics/logo.svg'

const Footer = () => {
    return <footer className="s-footer">
        <div className="container">
            <div className="row">
                <div className="col-12 col-md-auto">
                    <div className="s-footer__logo">
                        <img src={icon_logo} alt=""/>
                    </div>
                </div>
                <div className="col-12 col-md-auto">
                    <ul className="s-footer__menu">
                        <li><a target="_blank" rel="noopener noreferrer"
                               href="https://app.envelop.is/">dApps</a></li>
                        <li><a target="_blank" rel="noopener noreferrer"
                               href="https://nft2.envelop.is/">NFT 2.0</a></li>
                        <li><a target="_blank" rel="noopener noreferrer"
                               href="https://t.me/envelop_en">Support</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
}

export default Footer
